#Practico 4 Sudoku
#Alex Barria

#Se plantea la matriz del sudoku entregado, donde 0 representa el valor del lugar vacio
matriz = [
    [5,3,0,0,7,0,0,0,0],
    [6,0,0,1,9,5,0,0,0],
    [0,9,8,0,0,0,0,6,0],
    [8,0,0,0,6,0,0,0,3],
    [4,0,0,8,0,3,0,0,1],
    [7,0,0,0,2,0,0,0,6],
    [0,6,0,0,0,0,2,8,0],
    [0,0,0,4,1,9,0,0,5],
    [0,0,0,0,8,0,0,7,9]
]

#se recorre la matriz y se imprime
def print_matriz(matriz):
    for i in range(len(matriz)):
        if i % 3 == 0 and i != 0:
            print("- - - - - - - - - - - - - ")

        for j in range(len(matriz[0])):
            if j % 3 == 0 and j != 0:
                print(" | ", end="")

            if j == 8:
                print(matriz[i][j])
            else:
                print(str(matriz[i][j]) + " ", end="")

#a continuacion se describe el codigo que recorre la matriz
#buscando los espacios vacios
def find_empty(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[0])):
            if matriz[i][j] == 0:
                return i, j
    return None
#se validan el numero de correcto de filas y columnas con los correspondientes espacios vacios
def solve(matriz):
    find = find_empty(matriz)
    if not find:
        return True
    else:
        fila, columna = find

    for i in range(1, 10):
        if valid(matriz, i, (fila, columna)):
            matriz[fila][columna] = i

            if solve(matriz):
                return True

            matriz[fila][columna] = 0

    return False

#se compara la posicion y la validez de cada uno de los numeros
#respecto a las reglas del juego
def valid(matriz, number, position):
    if (check_row(matriz, number, position) and
            check_col(matriz, number, position) and
            check_box(matriz, number, position)):
        return True
    return False
#se recorren las filas y columnas para evaluar los numeros y rellenar con los valores adecuados
def check_row(matriz, number, position):
    for i in range(len(matriz[0])):
        if matriz[position[0]][i] == number and position[1] != i:
            return False
    return True
def check_col(matriz, number, position):
    for i in range(len(matriz)):
        if matriz[i][position[1]] == number and position[0] != i:
            return False
    return True
#se evaluan e insertan a nivel matricial los valores en las filas y las columnas
def check_box(matriz, number, position):
    valor_x = position[1] // 3
    valor_y = position[0] // 3

    for i in range(valor_y * 3, valor_y * 3 + 3):
        for j in range(valor_x * 3, valor_x * 3 + 3):
            if matriz[i][j] == number and (i, j) != position:
                return False
    return True

#finalmente se imprime la matriz con los valores finales
#--------------------------MAIN
solve(matriz)
print_matriz(matriz)


